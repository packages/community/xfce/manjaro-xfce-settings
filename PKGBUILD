# Maintainer: Philip Müller <philm@manjaro.org>
# Maintainer: Bernhard Landauer <oberon@manjaro.org>
# Contributor: Ramon Buldó

pkgbase=manjaro-xfce-settings
pkgname=('manjaro-xfce-settings'
         'manjaro-xfce-minimal-settings')
pkgver=20240216
pkgrel=1
pkgdesc="Manjaro Linux Xfce settings"
arch=('any')
url="https://gitlab.manjaro.org/profiles-and-settings/manjaro-xfce-settings"
license=('GPL')
depends=('illyria-wallpaper'
         'kvantum-theme-matcha'
         'manjaro-base-skel'
         'manjaro-icons'
         'matcha-gtk-theme'
         'noto-fonts'
         'papirus-maia-icon-theme'
         'qt5ct'
         'xcape'
         'xcursor-breeze')
makedepends=('git')
_commit=0ead589c1ef88aaeccbe060312d53b7d966d8349  # branch/master
source=("git+$url.git#commit=${_commit}"
        "${pkgbase}.hook"
        'mxs-bgd-sym'
        'xfce-pbw.sh'
        'xfce-panel-workaround.desktop')
sha256sums=('962f9f5ef8a9740ea89a7f4597f05ac1dd60a4b5584134a331b0543146426a11'
            'b18b45c0b88a4e282aa7f75caa5ab245443beba5de004042df3cf7361c1e4a2e'
            '03770c4735b588bdee49f7e1e5d73d8500524d875603c473bdf0f4ffb862c152'
            'c2a09b24e47df963fae456df3286e0f1525eb16b48da5a020579e788037631da'
            '3afbe1b3e8b8339ace5f8496f8fcd605d5768c10145da15a71a9d487be45a8c9')

pkgver() {
  cd "${pkgbase}"
  printf "$(git show -s --format=%cd --date=format:%Y%m%d HEAD)"
}

_inst_dir(){
  install -d "${pkgdir}/etc"
  cp -rL "${pkgbase}${1}/skel" "${pkgdir}/etc"

  install -d "${pkgdir}/usr/share/glib-2.0"
  cp -r "${pkgbase}/schemas" "${pkgdir}/usr/share/glib-2.0"
}

package_manjaro-xfce-settings() {
  pkgdesc="Manjaro Linux Xfce settings"
  provides=('manjaro-desktop-settings')
  conflicts=('manjaro-desktop-settings' 'manjaro-xfce-gtk3-settings')
  replaces=('manjaro-xfce-gtk3-settings')

  _inst_dir

  local _subst="
    s|%BIN%|mxs-bgd-sym|g
  "

  sed "${_subst}" "${pkgbase}.hook" |
    install -Dm644 /dev/stdin "${pkgdir}/usr/share/libalpm/hooks/${pkgbase}.hook"
  install -Dm755 mxs-bgd-sym "${pkgdir}/usr/bin/mxs-bgd-sym"
  install -Dm755 xfce-pbw.sh "${pkgdir}/etc/skel/.config/autostart/xfce-pbw.sh"
  install -Dm644 xfce-panel-workaround.desktop "${pkgdir}/etc/skel/.config/autostart/xfce-panel-workaround.desktop"
}

#############minimal######################

package_manjaro-xfce-minimal-settings() {
  pkgdesc="Manjaro Linux Xfce Minimal settings"
  provides=('manjaro-desktop-settings')
  conflicts=('manjaro-desktop-settings' 'manjaro-xfce-gtk3-minimal-settings')
  replaces=('manjaro-xfce-gtk3-minimal-settings')

  _inst_dir '/minimal'

  local _subst="
    s|%BIN%|mxms-bgd-sym|g
  "

  sed "${_subst}" "${pkgbase}.hook" |
    install -Dm644 /dev/stdin "${pkgdir}/usr/share/libalpm/hooks/manjaro-xfce-minimal-settings.hook"
  install -Dm755 mxs-bgd-sym "${pkgdir}/usr/bin/mxms-bgd-sym"
  install -Dm755 xfce-pbw.sh "${pkgdir}/etc/skel/.config/autostart/xfce-pbw.sh"
  install -Dm644 xfce-panel-workaround.desktop "${pkgdir}/etc/skel/.config/autostart/xfce-panel-workaround.desktop"
}
